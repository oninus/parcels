using System;

namespace Gaitcon.Parcels.Tester {
  public abstract class ParcelTester {
    #region Fields
    private ParcelModel _testParcel;
    private bool _isSuccess;
    private string _testName;
    #endregion
    
    #region Constructor
    public ParcelTester(string testName) {
      _testParcel = new ParcelModel();
      _isSuccess = false;
      _testName = testName;
    }
    #endregion

    #region Properties
    public ParcelModel TestParcel { 
      get { return _testParcel; } 
      set { _testParcel = value; } 
    }
    #endregion

    #region Methods
    protected abstract bool ExecuteTest();

    public abstract void Setup();  

    public void Test() {
      Console.WriteLine("--Start {0}--", _testName);

      _isSuccess = this.ExecuteTest();

      if (!_isSuccess)    
        Console.WriteLine("Test failed!");
      else
        Console.WriteLine("Test Successful!");

      Console.WriteLine("--End {0}--", _testName);
    }
    #endregion
  }
}