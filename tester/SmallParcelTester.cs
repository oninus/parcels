using System;

namespace Gaitcon.Parcels.Tester {
  public class SmallParcelTester:  ParcelTester {
    #region Constructor
    public SmallParcelTester(): base("SmallParcelTester") {}
    #endregion

    #region Methods
    protected override bool ExecuteTest()
    {
      ParcelBuilder builder = new ParcelBuilder(this.TestParcel);
      ParcelEvaluator smallParcelEval = new SmallParcelEvaluator(this.TestParcel, builder);
      ParcelEvaluator heavyParcelEval = new HeavyParcelEvaluator(this.TestParcel, 
        builder, smallParcelEval);
      ParcelEvaluator rejectParcelEval = new RejectedParcelEvaluator(this.TestParcel,
        builder, heavyParcelEval);     
      IParcel parcelResult = new InitParcel(this.TestParcel);
      bool result = false;

      rejectParcelEval.Evaluate();
      parcelResult = builder.GetResults();
      result = (parcelResult.GetCost() == 18M & 
        parcelResult.GetCostString() == "$18.00" &
          parcelResult.GetCategory() == "Small Parcel");

      return result;
    }

    public override void Setup()
    {
      this.TestParcel.Weight = 2M;
      this.TestParcel.Height = 3M;
      this.TestParcel.Width = 10M;
      this.TestParcel.Depth = 12;
    }
    #endregion
  }
}