using Gaitcon.Parcels;

namespace Gaitcon.Parcels.Tester {
  public class RejectedParcelTester:  ParcelTester {
    #region Constructor
    public RejectedParcelTester(): base("RejectedParcelTester") {}
    #endregion

    #region Methods
    protected override bool ExecuteTest()
    {
      ParcelBuilder builder = new ParcelBuilder(this.TestParcel);
      ParcelEvaluator rejectParcelEval = new RejectedParcelEvaluator(this.TestParcel,
        builder);
      IParcel parcelResult = new InitParcel(this.TestParcel);
      bool result = false;

      rejectParcelEval.Evaluate();
      parcelResult = builder.GetResults();
      result = (parcelResult.GetCost() == 0 & parcelResult.GetCostString() == "N/A" &
        parcelResult.GetCategory() == "Rejected");   

      return result;
    }

    public override void Setup()
    {
      this.TestParcel.Weight = 110M;
      this.TestParcel.Height = 20M;
      this.TestParcel.Width = 55M;
      this.TestParcel.Depth = 120M;
    }
    #endregion
  }
}