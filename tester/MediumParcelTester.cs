using Gaitcon.Parcels;

namespace Gaitcon.Parcels.Tester {
  public class MediumParcelTester:  ParcelTester {    
    #region Constructor
    public MediumParcelTester(): base("MediumParcelTester") {}
    #endregion

    #region Methods
    protected override bool ExecuteTest()
    {
      ParcelBuilder builder = new ParcelBuilder(this.TestParcel);
      ParcelEvaluator mediumParcelEval = new MediumParcelEvaluator(this.TestParcel, 
        builder);
      ParcelEvaluator smallParcelEval = new SmallParcelEvaluator(this.TestParcel, 
        builder, mediumParcelEval);
      ParcelEvaluator heavyParcelEval = new HeavyParcelEvaluator(this.TestParcel, 
        builder, smallParcelEval);
      ParcelEvaluator rejectParcelEval = new RejectedParcelEvaluator(this.TestParcel,
        builder, heavyParcelEval);     
      IParcel parcelResult = new InitParcel(this.TestParcel);
      bool result = false;

      rejectParcelEval.Evaluate();
      parcelResult = builder.GetResults();
      result = (parcelResult.GetCost() == 80M & 
        parcelResult.GetCostString() == "$80.00" &
          parcelResult.GetCategory() == "Medium Parcel");

      return result;
    }

    public override void Setup()
    {
      this.TestParcel.Weight = 10M;
      this.TestParcel.Height = 20M;
      this.TestParcel.Width = 5M;
      this.TestParcel.Depth = 20M;
    }
    #endregion
  }
}