using Gaitcon.Parcels;

namespace Gaitcon.Parcels.Tester {    
  public class LargeParcelTester:  ParcelTester {
    #region Constructor
    public LargeParcelTester(): base("LargeParcelTester") {}
    #endregion

    #region Methods
    protected override bool ExecuteTest()
    {
      ParcelBuilder builder = new ParcelBuilder(this.TestParcel);
      ParcelEvaluator largeParcelEval = new LargeParcelEvaluator(this.TestParcel, 
        builder);
      ParcelEvaluator mediumParcelEval = new MediumParcelEvaluator(this.TestParcel, 
        builder, largeParcelEval);
      ParcelEvaluator smallParcelEval = new SmallParcelEvaluator(this.TestParcel, 
        builder, mediumParcelEval);
      ParcelEvaluator heavyParcelEval = new HeavyParcelEvaluator(this.TestParcel, 
        builder, smallParcelEval);
      ParcelEvaluator rejectParcelEval = new RejectedParcelEvaluator(this.TestParcel,
        builder, heavyParcelEval);     
      IParcel parcelResult = new InitParcel(this.TestParcel);
      bool result = false;

      rejectParcelEval.Evaluate();
      parcelResult = builder.GetResults();
      result = (parcelResult.GetCost() == (0.03M * this.TestParcel.Volume)  & 
        parcelResult.GetCategory() == "Large Parcel");

      return result;
    }

    public override void Setup()
    {
      this.TestParcel.Weight = 10M;
      this.TestParcel.Height = 200M;
      this.TestParcel.Width = 550M;
      this.TestParcel.Depth = 1200M;
    }
    #endregion
  }
}