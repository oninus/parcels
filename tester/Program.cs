﻿using System;
using System.Collections.Generic;

namespace Gaitcon.Parcels.Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--Test Start--");
            
            List<ParcelTester> testers = new List<ParcelTester>();

            testers.Add(new MediumParcelTester());
            testers.Add(new LargeParcelTester());
            testers.Add(new SmallParcelTester());
            testers.Add(new HeavyParcelTester());
            testers.Add(new RejectedParcelTester());

            foreach(ParcelTester test in testers) {
                test.Setup();
                test.Test();
            }

            Console.WriteLine("--Test End--");            
        }
    }
}
