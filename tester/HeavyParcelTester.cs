using System.Collections.Generic;
using Gaitcon.Parcels;

namespace Gaitcon.Parcels.Tester {
  public class HeavyParcelTester:  ParcelTester {
    #region Constructor
    public HeavyParcelTester(): base("HeavyParcelTester") {}
    #endregion

    #region Methods
    protected override bool ExecuteTest()
    {
      ParcelBuilder builder = new ParcelBuilder(this.TestParcel);
      ParcelEvaluator heavyParcelEval = new HeavyParcelEvaluator(this.TestParcel, 
        builder);
      ParcelEvaluator rejectParcelEval = new RejectedParcelEvaluator(this.TestParcel,
        builder, heavyParcelEval);     
      IParcel parcelResult = new InitParcel(this.TestParcel);
      bool result = false;

      rejectParcelEval.Evaluate();
      parcelResult = builder.GetResults();
      result = (parcelResult.GetCost() == 330M & 
        parcelResult.GetCostString() == "$330.00" &
          parcelResult.GetCategory() == "Heavy Parcel");

      return result;
    }

    public override void Setup()
    {
      this.TestParcel.Weight = 22M;
      this.TestParcel.Height = 5M;
      this.TestParcel.Width = 5M;
      this.TestParcel.Depth = 5M;
    }
    #endregion
  }
}