import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ParcelService } from "../service/parcel.service";
import { ParcelMeasurement } from "../models/ParcelMeasurement";
import { ParcelResult } from "../models/ParcelResult";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  encapsulation:  ViewEncapsulation.ShadowDom
})
export class HomeComponent implements OnInit {
  parcelMeasurements:  ParcelMeasurement;
  parcelResult:  ParcelResult;
  notice:  string;  

  // constructor(private parcelService:  ParcelService, private fb:  FormBuilder) {
  //   this.form = this.fb.group({
  //     weight:  [
  //       '', 
  //       [Validators.pattern('^[0-9]*$'), Validators.min(0), Validators.max(9999999)]
  //     ]
  //   });
  //}

  constructor(private parcelService:  ParcelService) {}

  ngOnInit():  void {
    this.parcelMeasurements = new ParcelMeasurement();
    this.parcelResult = new ParcelResult();
    this.notice = '';
  }

  private validate(val:  number):  boolean {
    let isValid = false;

    if (val > 0 && val <= 9999)
      isValid = true;
    else
      isValid = false;

    return isValid;
  }

  public evaluate() {
    this.notice = '';
    
    if (
      this.validate(this.parcelMeasurements.weight) &&
      this.validate(this.parcelMeasurements.height) &&
      this.validate(this.parcelMeasurements.width) &&
      this.validate(this.parcelMeasurements.weight)
    ) {
      this.parcelResult = new ParcelResult();
      this.parcelService.evaluate(this.parcelMeasurements).subscribe(
        data => { this.parcelResult = data },
        err => { this.notice = err }
      );
    } else {
      this.notice = 'Values must be between 1-9999!';
    }

    
  }

}
