import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, shareReplay, map } from "rxjs/operators";
import { ParcelMeasurement } from '../models/ParcelMeasurement';
import { ParcelResult } from "../models/ParcelResult";

@Injectable()
export class ParcelService {
  private url:  string;
  private httpOptions:  any;

  constructor(private http:  HttpClient, @Inject('BASE_URL') baseUrl: string) { 
    this.httpOptions = {
      headers:  new HttpHeaders({
        'Content-Type':'application/json'
      })
    };

    this.url = baseUrl;
  }

  private handleError(error:  HttpErrorResponse) {
    let msg:  string;

    if (error.error instanceof ErrorEvent) {
      msg = 'Client side error:  ' + error.message;
    } else {
      if (error.status == 401)
        msg = 'Invalid username/password!!!';
      else
        msg = 'Server side error:  ' + error.message;
    }    
    
    return throwError(msg)
  }

  public evaluate(data:  ParcelMeasurement):  Observable<any> {
    return this.http.post(
      this.url + 'parcel/', 
      '{ ' + 
        '"weight":' + data.weight + ',' +
        '"height":' + data.height + ',' +
        '"width":' + data.width + ',' +
        '"depth":' + data.depth + ',' +
        '"isEvaluated":false' + 
      ' }',
      this.httpOptions
    ).pipe(      
      catchError(err => this.handleError(err)), 
      shareReplay()
    );
  }
}
