export class ParcelMeasurement {
  public weight:  number;
  public height:  number;
  public width:  number;
  public depth:  number;

  constructor() {
    this.weight = 0;
    this.height = 0;
    this.width = 0;
    this.depth = 0;
  }
}