import { ParcelMeasurement } from "./ParcelMeasurement";

export class ParcelResult {
  public model:  ParcelMeasurement;
  public category:  string;
  public cost:  string;
  
  constructor() {
    this.model = new ParcelMeasurement();
    this.category = '';
    this.cost = '';
  }
}