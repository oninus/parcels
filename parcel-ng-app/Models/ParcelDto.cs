using Gaitcon.Parcels;

namespace parcel_ng_app.Models
{
    public class ParcelDto {
        #region Properties
        public ParcelModel Model { get; set; }

        public string Category { get; set; }

        public string Cost { get; set; }
        #endregion

        #region Constructor
        public ParcelDto() {
            Model = new ParcelModel();
            Category = string.Empty;
            Cost = string.Empty;
        }
        #endregion
    }
}