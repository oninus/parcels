using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Gaitcon.Parcels;
using parcel_ng_app.Models;

namespace parcel_ng_app.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParcelController : ControllerBase
    {
        
        private readonly ILogger<WeatherForecastController> _logger;

        public ParcelController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public ParcelDto Get()
        {
            return new ParcelDto();
        }

        [HttpPost]
        public ParcelDto Post(ParcelModel model)
        {
            ParcelDto result = new ParcelDto();
            ParcelBuilder builder = new ParcelBuilder(model);
            ParcelEvaluator largeParcelEval = new LargeParcelEvaluator(model, 
                builder);
            ParcelEvaluator mediumParcelEval = new MediumParcelEvaluator(model, 
                builder, largeParcelEval);
            ParcelEvaluator smallParcelEval = new SmallParcelEvaluator(model, 
                builder, mediumParcelEval);
            ParcelEvaluator heavyParcelEval = new HeavyParcelEvaluator(model, 
                builder, smallParcelEval);
            ParcelEvaluator rejectParcelEval = new RejectedParcelEvaluator(
                model, builder, heavyParcelEval);     
            IParcel parcelResult = new InitParcel(model);            

            rejectParcelEval.Evaluate();
            parcelResult = builder.GetResults();

            result.Model = parcelResult.Model;
            result.Category = parcelResult.GetCategory();
            result.Cost = parcelResult.GetCostString();            

            return result;
        }
    }
}
