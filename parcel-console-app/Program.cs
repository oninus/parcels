﻿using System;

namespace Gaitcon.Parcels.ConsoleApp
{
    class Program
    {
        static IParcel EvaluateParcel(ParcelModel model) 
        {
            ParcelBuilder builder = new ParcelBuilder(model);
            ParcelEvaluator largeParcelEval = new LargeParcelEvaluator(model, 
                builder);
            ParcelEvaluator mediumParcelEval = new MediumParcelEvaluator(model, 
                builder, largeParcelEval);
            ParcelEvaluator smallParcelEval = new SmallParcelEvaluator(model, 
                builder, mediumParcelEval);
            ParcelEvaluator heavyParcelEval = new HeavyParcelEvaluator(model, 
                builder, smallParcelEval);
            ParcelEvaluator rejectParcelEval = new RejectedParcelEvaluator(model,
                builder, heavyParcelEval);     
            IParcel parcelResult = new InitParcel(model);            

            rejectParcelEval.Evaluate();
            parcelResult = builder.GetResults();

            return parcelResult;
        }

        static void Main(string[] args)
        {
            ConsoleView view = new ConsoleView();            

            ParcelModel model = view.Prompt();
            view.NotifyProcessing();
            view.Output(EvaluateParcel(model));

        }
    }
}
