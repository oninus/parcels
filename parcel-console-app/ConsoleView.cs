using System;
using Gaitcon.Parcels;

namespace Gaitcon.Parcels.ConsoleApp
{   
    public class ConsoleView
    {
        #region Constructors
        public ConsoleView() {}
        #endregion

        #region Methods
        private decimal GetValue(string input) {
          decimal value = 0M;
          decimal result = 0M;          

          if (decimal.TryParse(input, out value))
            if (value > 0 & value <= 9999M)
              result = value;
            else
              Console.WriteLine("Input value must be a valid decimal [1-9999M]!");
          else
            Console.WriteLine("Input value must be a valid decimal [1-9999M]!");            

          return result;
        }

        private decimal PromptInputItem(string label)
        {         
          decimal value = 0M;
          string input = string.Empty;

          while(value == 0)
          {
            Console.Write(label);
            input = Console.ReadLine();
            value = GetValue(input);
          }

          return value;
        }

        public ParcelModel Prompt() {
          string weightLabel = "Enter Weight:  ";
          string heightLabel = "Enter Height:  ";
          string widthLabel = "Enter Width:  ";
          string depthLabel = "Enter Depth:  ";
          ParcelModel model = new ParcelModel();
          
          Console.WriteLine("--Gaitcon Parcels Prompt--");
          model.Weight = PromptInputItem(weightLabel);
          model.Height = PromptInputItem(heightLabel);
          model.Width = PromptInputItem(widthLabel);
          model.Depth = PromptInputItem(depthLabel);
          Console.WriteLine("--Finished Prompt--");

          return model;
        }

        public void NotifyProcessing()
        {
          Console.WriteLine("Processing...");
        }

        public void Output(IParcel parcel) {
          Console.WriteLine("--Gaitcon Parcels Output--");
          Console.WriteLine("Category:  {0}", parcel.GetCategory());
          Console.WriteLine("Cost:  {0}", parcel.GetCostString());
          Console.WriteLine("--Finished Output--");
        }
        #endregion
    }
}