namespace Gaitcon.Parcels
{
    public interface IParcel
    {
        #region Properties
        ParcelModel Model { get; }
        #endregion      
        #region Methods
        decimal GetCost();

        string GetCostString();

        string GetCategory();     
        #endregion        
    }
}