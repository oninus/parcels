namespace Gaitcon.Parcels {
  public abstract class ParcelEvaluator
  {
    #region Fields
    private ParcelEvaluator _next;
    private ParcelBuilder _builder;
    #endregion

    #region Constructor
    public ParcelEvaluator(ParcelBuilder builder, ParcelEvaluator next) {
      _next = next;
      _builder = builder;
    }

    public ParcelEvaluator(ParcelBuilder builder):  this(builder, null) {}
    #endregion

    #region Properties
    protected ParcelEvaluator NextEvaluator {
      get { return _next; }
    }

    public abstract bool Pass { get; }

    public abstract ParcelModel Parcel { get; }

    public ParcelBuilder Builder { get { return _builder; }}
    #endregion    

    #region Methods
    public abstract void Evaluate();    
    #endregion
  }
}