namespace Gaitcon.Parcels
{
    public class InitParcel:  IParcel {
      #region Fields
      private ParcelModel _model;
      #endregion

      #region Constructors
      public InitParcel(ParcelModel model) {
        _model = model;
      }
      #endregion

      #region Properties
      public ParcelModel Model { get { return _model; } }    
      #endregion

      #region Methods


      public decimal GetCost() {
        return 0M;
      }

      public string GetCostString() {

        return "Unknown cost";
      }

      public string GetCategory() {
        
        return "Uncategorized";
      }
      #endregion
    }
}