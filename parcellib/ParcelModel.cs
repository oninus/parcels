﻿using System;

namespace Gaitcon.Parcels
{
    public class ParcelModel
    {
        #region Fields
        private decimal _weight;
        private decimal _height;
        private decimal _width;
        private decimal _depth;
        private bool _isEvaluated;
        #endregion

        #region Construtor
        public ParcelModel() {
            _weight = 0M;
            _height = 0M;
            _width = 0M;
            _depth = 0M;            
            _isEvaluated = false;
        }
        #endregion

        #region Properties
        public decimal Weight {
            get { return _weight; }
            set {_weight = value;}
        }

        public decimal Height { 
            get { return _height; } 
            set { _height = value; } 
        }

        public decimal Width { 
            get { return _width; } 
            set { _width = value; }
        }

        public decimal Depth { 
            get { return _depth; } 
            set { _depth = value; }
        }

        public decimal Volume { 
            get { return _height * _width *_depth; }  
        }

        public bool IsEvaluated { 
            get { return _isEvaluated; } 
            set { _isEvaluated = value; }
        }
        #endregion
    }
}
