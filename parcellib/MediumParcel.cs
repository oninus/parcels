namespace Gaitcon.Parcels
{
    public class MediumParcel:  IParcel {
      #region Fields
      private ParcelModel _model;
      #endregion

      #region Constructors
      public MediumParcel(ParcelModel model) {
        _model = model;
      }
      #endregion

      #region Properties
      public ParcelModel Model { get { return _model; } }    
      #endregion

      #region Methods


      public decimal GetCost() {
        return 0.04M * this.Model.Volume;
      }

      public string GetCostString() {
        return string.Format("${0:0.00}", GetCost());
      }

      public string GetCategory() {
        
        return "Medium Parcel";
      }
      #endregion
    }
}