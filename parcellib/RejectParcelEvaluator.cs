namespace Gaitcon.Parcels
{
    public class RejectedParcelEvaluator: ParcelEvaluator {
      #region Fields
      private bool _pass;
      private ParcelModel _model;          
      #endregion

      #region Constructor
      public RejectedParcelEvaluator(ParcelModel model, ParcelBuilder builder, 
        ParcelEvaluator next): base(builder, next) 
      {
        _pass = false;
        _model = model;
      }

      public RejectedParcelEvaluator(ParcelModel model, ParcelBuilder builder):
        this(model, builder, null) {}
      #endregion

      #region Properties
      public override bool Pass { get { return _pass; } }          

      public override ParcelModel Parcel { get { return _model; } }
      #endregion

      #region Methods
      public override void Evaluate() {
        if (!this.Parcel.IsEvaluated) {
          if (_model.Weight > 50M) {
            _pass = true;
            _model.IsEvaluated = true;
            this.Builder.CreateRejectedParcel();
          }

          if (!_pass) {
            if (this.NextEvaluator != null)
              this.NextEvaluator.Evaluate();          
          }  
        }
      }      
      #endregion      
    }
}