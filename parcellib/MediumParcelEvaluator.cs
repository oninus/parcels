namespace Gaitcon.Parcels
{
    public class MediumParcelEvaluator: ParcelEvaluator {
      #region Fields
      private bool _pass;
      private ParcelModel _model;          
      #endregion

      #region Constructor
      public MediumParcelEvaluator(ParcelModel model, ParcelBuilder builder, 
        ParcelEvaluator next):  base(builder, next) 
      {
        _pass = false;
        _model = model;
      }

      public MediumParcelEvaluator(ParcelModel model, ParcelBuilder builder):
        this(model, builder, null) {}
      #endregion

      #region Properties
      public override bool Pass { get { return _pass; } }          

      public override ParcelModel Parcel { get { return _model; } }
      #endregion

      #region Methods
      public override void Evaluate() {
        if (!this.Parcel.IsEvaluated) {
          if (_model.Volume >= 1500 & _model.Volume < 2500) {
            _pass = true;
            _model.IsEvaluated = true;
            this.Builder.CreateMediumParcel();
          }

          if (!_pass) {
            if (this.NextEvaluator != null)
              this.NextEvaluator.Evaluate();          
          }
        }
      }      
      #endregion      
    }
}