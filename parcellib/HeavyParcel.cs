namespace Gaitcon.Parcels
{
    public class HeavyParcel:  IParcel {
      #region Fields
      private ParcelModel _model;
      #endregion

      #region Constructors
      public HeavyParcel(ParcelModel model) {
        _model = model;
      }
      #endregion

      #region Properties
      public ParcelModel Model { get { return _model; } }    
      #endregion

      #region Methods


      public decimal GetCost() {
        return 15M * this.Model.Weight;
      }

      public string GetCostString() {
        return string.Format("${0:0.00}", GetCost());
      }

      public string GetCategory() {
        
        return "Heavy Parcel";
      }
      #endregion
    }
}