namespace Gaitcon.Parcels
{
    public class ParcelBuilder {
      #region Fields
      private IParcel _result;
      private ParcelModel _model;
      #endregion

      #region Constructor
      public ParcelBuilder(ParcelModel model) {
        _result = new InitParcel(model);
        _model = model;
      }
      #endregion

      #region Methods
      public void CreateRejectedParcel() {
        _result = new RejectedParcel(_model);
      }

      public void CreateHeavyParcel() {
        _result = new HeavyParcel(_model);
      }

      public void CreateSmallParcel() {
        _result = new SmallParcel(_model);
      }

      public void CreateMediumParcel() {
        _result = new MediumParcel(_model);
      }

      public void CreateLargeParcel() {
        _result = new LargeParcel(_model);
      }

      public IParcel GetResults() {
        return _result;
      }
      #endregion
    }
}