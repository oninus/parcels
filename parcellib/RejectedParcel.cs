namespace Gaitcon.Parcels
{
    public class RejectedParcel:  IParcel {
      #region Fields
      private ParcelModel _model;
      #endregion

      #region Constructors
      public RejectedParcel(ParcelModel model) {
        _model = model;
      }
      #endregion

      #region Properties
      public ParcelModel Model { get { return _model; } }    
      #endregion

      #region Methods


      public decimal GetCost() {
        return 0M;
      }

      public string GetCostString() {
        return "N/A";
      }

      public string GetCategory() {
        return "Rejected";
      }
      #endregion
    }
}