namespace Gaitcon.Parcels
{
    public class LargeParcel:  IParcel {
      #region Fields
      private ParcelModel _model;
      #endregion

      #region Constructors
      public LargeParcel(ParcelModel model) {
        _model = model;
      }
      #endregion

      #region Properties
      public ParcelModel Model { get { return _model; } }    
      #endregion

      #region Methods


      public decimal GetCost() {
        return 0.03M * this.Model.Volume;
      }

      public string GetCostString() {
        return string.Format("${0:0.00}", GetCost());
      }

      public string GetCategory() {
        
        return "Large Parcel";
      }
      #endregion
    }
}